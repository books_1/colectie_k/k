# K

## Content

```
./K. K. Rokossovski:
K. K. Rokossovski - Datoria ostaseasca 1.0 '{Razboi}.docx

./K. Kwasniewski:
K. Kwasniewski - Voi pune actorii sa repete 1.0 '{Politista}.docx

./K. Moiseeva:
K. Moiseeva - Fica lui Ekhnaton 1.0 '{AventuraIstorica}.docx

./K. Stockett:
K. Stockett - Culoarea sentimentelor 1.0 '{Literatura}.docx

./Kabder Abdola:
Kabder Abdola - La portile moscheii 0.7 '{Literatura}.docx

./Kage Baker:
Kage Baker - Acolo unde cresc merele aurii 2.0 '{SF}.docx

./Kahlil Gibran:
Kahlil Gibran - Iisus, fiul omului 0.6 '{Spiritualitate}.docx
Kahlil Gibran - Profetul 0.99 '{Spiritualitate}.docx

./Kai Meyer:
Kai Meyer - Cruciada fecioarelor V1 1.0 '{AventuraIstorica}.docx
Kai Meyer - Cruciada fecioarelor V2 1.0 '{AventuraIstorica}.docx

./Kalman Mikszath:
Kalman Mikszath - Umbrela sfantului Petru 1.0 '{Literatura}.docx

./Kami Garcia & Margaret Stohl:
Kami Garcia & Margaret Stohl - Cronicile Castelanilor - V1 A 16-a luna 1.0 '{Supranatural}.docx
Kami Garcia & Margaret Stohl - Cronicile Castelanilor - V2 Cartea lunilor 1.0 '{Supranatural}.docx
Kami Garcia & Margaret Stohl - Cronicile Castelanilor - V3 Luna de foc 1.0 '{Supranatural}.docx

./Karel Capek:
Karel Capek - Fabrica de absolut 1.0 '{SF}.docx
Karel Capek - Krakatit 2.0 '{SF}.docx

./Karel Fabian:
Karel Fabian - Lup-Negru 1.0 '{Politista}.docx

./Karen Cleveland:
Karen Cleveland - Acces interzis 1.0 '{Dragoste}.docx

./Karen Dionne:
Karen Dionne - Evadarea din mlastina 1.0 '{Literatura}.docx

./Karen Joy Fowler:
Karen Joy Fowler - Ne-am iesit cu totii complet din minti 1.0 '{Literatura}.docx

./Karen Levine:
Karen Levine - Geamantanul Hanei 0.99 '{Literatura}.docx

./Karen Mack & Jennifer Kaufman:
Karen Mack & Jennifer Kaufman - Amanta lui Freud 1.0 '{Literatura}.docx

./Karen Maitland:
Karen Maitland - Calatoria mincinosilor 1.0 '{Literatura}.docx

./Karen Marie Moning:
Karen Marie Moning - Mackayla Lane - V1 Febra neagra 1.0 '{Supranatural}.docx

./Karen McManus:
Karen McManus - Unul dintre noi minte 1.0 '{Thriller}.docx

./Karen O'connell:
Karen O'connell - Adevarata prietenie 0.99 '{Romance}.docx
Karen O'connell - Datorita iubirii 0.9 '{Dragoste}.docx

./Karen Reynolds:
Karen Reynolds - Dragoste umilitoare 0.9 '{Dragoste}.docx
Karen Reynolds - Un zambet scaldat in lacrimi 0.9 '{Romance}.docx

./Karen Rose:
Karen Rose - Nu te poti ascunde 0.99 '{Thriller}.docx

./Karen Rose Smith:
Karen Rose Smith - Craciun in Spania 0.9 '{Dragoste}.docx

./Karen Whiddon:
Karen Whiddon - Ghinionista 0.2 '{Dragoste}.docx
Karen Whiddon - O casatorie surpriza 0.9 '{Dragoste}.docx
Karen Whiddon - Zidul prabusit 0.9 '{Dragoste}.docx

./Karin Fossum:
Karin Fossum - Nu privi inapoi 1.0 '{Thriller}.docx

./Karin Slaughter:
Karin Slaughter - Grant County - V1 Furie oarba 1.0 '{Thriller}.docx
Karin Slaughter - Grant County - V2 Post mortem 1.0 '{Thriller}.docx
Karin Slaughter - Grant County - V3 Obsesia 1.0 '{Thriller}.docx
Karin Slaughter - Will Trent - V1 Triptic 1.0 '{Thriller}.docx
Karin Slaughter - Will Trent - V2 Suflete pustiite 1.0 '{Thriller}.docx
Karin Slaughter - Will Trent - V3 Geneza 1.0 '{Thriller}.docx
Karin Slaughter - Will Trent - V4 Vise spulberate 1.0 '{Thriller}.docx
Karin Slaughter - Will Trent - V5 Decaderea 1.0 '{Thriller}.docx

./Karinthy Frigyes:
Karinthy Frigyes - Calatorie in Faremido Capillaria 0.7 '{Literatura}.docx
Karinthy Frigyes - Dati-mi voie domnule profesor 1.0 '{Umor}.docx
Karinthy Frigyes - Mai 0.99 '{SF}.docx
Karinthy Frigyes - Moartea hipnotica 2.0 '{SF}.docx
Karinthy Frigyes - Ora de stiintele naturii la Scoala Ingerilor 0.99 '{SF}.docx

./Karlfried Graf Durckheim:
Karlfried Graf Durckheim - Extras 0.9 '{Spiritualitate}.docx
Karlfried Graf Durckheim - Goettmann. dialog pe calea initiatica 0.99 '{Spiritualitate}.docx

./Karl May:
Karl May - De pe tron la esafod - V1 Castelul Rodriganda 2.0 '{Western}.docx
Karl May - De pe tron la esafod - V2 Piamida zeului soare 2.0 '{Western}.docx
Karl May - De pe tron la esafod - V3 Benito Juarez 2.0 '{Western}.docx
Karl May - De pe tron la esafod - V4 Plisc de uliu 2.0 '{Western}.docx
Karl May - De pe tron la esafod - V5 Moartea imparatului 2.0 '{Western}.docx
Karl May - Dragostea ulanului - V1 Ultima iubire a lui Napoleon 2.0 '{Western}.docx
Karl May - Dragostea ulanului - V2 Razbunatorii 2.0 '{Western}.docx
Karl May - Dragostea ulanului - V3 Capitanul garzii imperiale 2.0 '{Western}.docx
Karl May - Dragostea ulanului - V4 Nelegiuitul 2.0 '{Western}.docx
Karl May - Dragostea ulanului - V5 Glasul sangelui 2.0 '{Western}.docx
Karl May - Fiul vanatorului de ursi 2.1 '{Western}.docx
Karl May - Groaznica moarte a lui Old Dursing Dry 2.1 '{Western}.docx
Karl May - In Anzii Cordilieri 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V1 In tara leului argintiu - V1 Leul razbunarii 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V2 In tara leului argintiu - V2 La turnul Babel 3.0 '{Western}.docx
Karl May - Kara ben Nemsi - V3 In tara leului argintiu - V3 Sub aripa mortii 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V4 In tara leului argintiu - V4 Prabusirea 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V5 In tara Mahdiului - V1 In tara Mahdiului 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V6 In tara Mahdiului - V2 Lacrimi si sange 1.9 '{Western}.docx
Karl May - Kara ben Nemsi - V7 In tara Mahdiului - V3 Ultima vanatoare de sclavi 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V8 Orient - V1 Prin desert si harem 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V9 Orient - V2 Prin Kurdistanul salbatic 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V10 Orient - V3 de la Bagdad la Stambul 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V11 Orient - V4 Prin vagaunile balcanilor 2.0 '{Western}.docx
Karl May - Kara ben Nemsi - V12 Orient - V5 In tara schipetarilor 3.0 '{Western}.docx
Karl May - Kara ben Nemsi - V13 Orient - V6 Schut, capetenia talharilor 2.0 '{Western}.docx
Karl May - Slujitorii mortii 2.0 '{Western}.docx
Karl May - Spiritul din Llano Estacado 2.0 '{Western}.docx
Karl May - Testamentul incasului 2.0 '{Western}.docx
Karl May - Tigancile - V1 Secretul tigancii 2.0 '{Western}.docx
Karl May - Tigancile - V2 Insula giuvaierelor 2.0 '{Western}.docx
Karl May - Winnetou - La San Francisco 2.0 '{Western}.docx
Karl May - Winnetou - Talharii pustiului V2 1.0 '{Western}.docx
Karl May - Winnetou - V1 De doua ori in ghearele mortii 2.0 '{Western}.docx
Karl May - Winnetou - V2 Pe viata si pe moarte 2.0 '{Western}.docx
Karl May - Winnetou - V3 Testamentul lui Winnetou 2.0 '{Western}.docx
Karl May - Winnetou - V4 Pirat si corsar 2.0 '{Western}.docx
Karl May - Winnetou - V5 Mustangul negru 2.0 '{Western}.docx
Karl May - Winnetou - V6 Comoara din Lacul de Argint 2.0 '{Western}.docx
Karl May - Winnetou - V7 Cacealmaua 2.0 '{Western}.docx
Karl May - Winnetou - V8 Inimi germane - V1 Dervisul 2.0 '{Western}.docx
Karl May - Winnetou - V9 Inimi germane - V2 Valea mortii 2.0 '{Western}.docx
Karl May - Winnetou - V10 Inimi germane - V3 Vanatorul de samur 2.0 '{Western}.docx
Karl May - Winnetou - V11 Vulturii desertului 2.0 '{Western}.docx
Karl May - Winnetou - V12 Old Surehand - V1 Old Surehand 2.0 '{Western}.docx
Karl May - Winnetou - V13 Old Surehand - V2 Taina lui Old Surehand 2.0 '{Western}.docx
Karl May - Winnetou - V14 Satan si Iscariotul - V1 Capcana 2.0 '{Western}.docx
Karl May - Winnetou - V15 Satan si Iscariotul - V2 Omul cu 12 degete 2.0 '{Western}.docx
Karl May - Winnetou - V16 Satan si Iscariotul - V3 Razbunarea 2.0 '{Western}.docx
Karl May - Winnetou - V17 Urmasii lui Winnetou 2.0 '{Western}.docx

./Karl Von Vereiter:
Karl Von Vereiter - Sina insangerata 1.0 '{ActiuneRazboi}.docx

./Karol Wojtyla:
Karol Wojtyla - Omul sfarsitului de mileniu 0.99 '{Religie}.docx

./Kassak Fred:
Kassak Fred - Duminica nu se fac inmormantari 1.0 '{Politista}.docx

./Kass Morgan:
Kass Morgan - Cei 100 - V1 Cei 100 3.0 '{SF}.docx
Kass Morgan - Cei 100 - V2 Ziua 21 1.0 '{SF}.docx
Kass Morgan - Cei 100 - V3 Intoarcerea acasa 1.0 '{SF}.docx

./Kate Atkinson:
Kate Atkinson - In culisele muzeului 0.99 '{Literatura}.docx

./Kate Douglas:
Kate Douglas - O licitatie neobisnuita 0.99 '{Romance}.docx

./Kate Freiman:
Kate Freiman - Golful de argint 0.9 '{Dragoste}.docx
Kate Freiman - San Francisco dupa furtuna 0.9 '{Romance}.docx
Kate Freiman - Un roman cu dedicatie 1.0 '{Dragoste}.docx
Kate Freiman - Zeita cu parul de aur 0.99 '{Dragoste}.docx

./Kate Hamer:
Kate Hamer - Fata cu palton rosu 1.0 '{Literatura}.docx

./Kate Holmes:
Kate Holmes - Castele de nisip 0.99 '{Dragoste}.docx

./Kate Morton:
Kate Morton - Casa de langa lac 1.0 '{Literatura}.docx
Kate Morton - Casa de la Riverton 2.0 '{Literatura}.docx
Kate Morton - Fiica ceasornicarului 1.0 '{Literatura}.docx
Kate Morton - Gradina uitata 1.0 '{Literatura}.docx
Kate Morton - Orele indepartate 1.0 '{Literatura}.docx
Kate Morton - Pazitoarea tainei 1.0 '{Literatura}.docx

./Kate Nevins:
Kate Nevins - Fidelitate si recunostinta 0.2 '{Romance}.docx
Kate Nevins - Rasarit de soare la Venetia 0.9 '{Dragoste}.docx

./Kate Norway:
Kate Norway - Cerere de divort 0.99 '{Dragoste}.docx

./Kate Quinn:
Kate Quinn - Reteaua Alice 1.0 '{Suspans}.docx

./Kate Russell:
Kate Russell - Vanessa mea cea intunecata 1.0 '{Literatura}.docx

./Kate Star:
Kate Star - In numele prieteniei 0.9 '{Dragoste}.docx
Kate Star - Intrusa 0.99 '{Dragoste}.docx
Kate Star - Medicul vaporului 0.99 '{Dragoste}.docx

./Kate Wilhelm:
Kate Wilhelm - Unde candva, suave pasari cantatoare 2.0 '{SF}.docx

./Katharina Hacker:
Katharina Hacker - Suflete pustii 0.99 '{Literatura}.docx

./Katharine McGee:
Katharine McGee - The Thousandth Floor - V1 Etajul o mie 1.0 '{SF}.docx
Katharine McGee - The Thousandth Floor - V2 Vertigo 1.0 '{SF}.docx

./Katheleen Eagle:
Katheleen Eagle - La fel ca altadata 0.99 '{Dragoste}.docx

./Katherine Granger:
Katherine Granger - Exista a doua sansa 0.99 '{Romance}.docx
Katherine Granger - Intalnirea cu fostul sot 0.9 '{Romance}.docx

./Katherine Kurtz:
Katherine Kurtz - Initiatul 1.0 '{Supranatural}.docx

./Katherine Neville:
Katherine Neville - Focul 1.0 '{Thriller}.docx
Katherine Neville - Opt 1.0 '{Thriller}.docx

./Katherine Pancol:
Katherine Pancol - Josephine - V1 Ochii galbeni ai crocodililor 1.0 '{Literatura}.docx
Katherine Pancol - Josephine - V2 Valsul lent al testoaselor 1.0 '{Literatura}.docx

./Katherine Roberts:
Katherine Roberts - Cele Sapte Minuni - V1 Pradarea marii piramide 1.0 '{SF}.docx
Katherine Roberts - Cele Sapte Minuni - V2 Intamplari din Babilon 1.0 '{SF}.docx
Katherine Roberts - Cele Sapte Minuni - V3 In cautarea templului amazoanelor 1.0 '{SF}.docx
Katherine Roberts - Cele Sapte Minuni - V4 Mausoleul din Halicarnas 1.0 '{SF}.docx
Katherine Roberts - Cele Sapte Minuni - V5 Blestemul din Olympia 1.0 '{SF}.docx

./Katherine Stone:
Katherine Stone - Sarpele din paradis 0.9 '{Dragoste}.docx

./Katherine Webb:
Katherine Webb - Mostenirea 1.0 '{Literatura}.docx

./Kathleen Glasgow:
Kathleen Glasgow - O fata din bucati 1.0 '{Literatura}.docx

./Kathleen Grissom:
Kathleen Grissom - Bucataria sclavilor 1.0 '{Literatura}.docx

./Kathleen McGowan:
Kathleen McGowan - Maria Magdalena - V1 Cea asteptata 1.0 '{AventuraIstorica}.docx
Kathleen McGowan - Maria Magdalena - V2 Cartea iubirii 1.0 '{AventuraIstorica}.docx
Kathleen McGowan - Maria Magdalena - V3 Printul poet 1.0 '{AventuraIstorica}.docx

./Kathleen O'Neal & W. Michael Gear:
Kathleen O'Neal & W. Michael Gear - Tradarea 1.0 '{AventuraIstorica}.docx

./Kathleen Tessaro:
Kathleen Tessaro - Colectionara de parfumuri interzise 0.9 '{Dragoste}.docx

./Kathleen Wayne:
Kathleen Wayne - Magia unei iubiri 0.8 '{Dragoste}.docx

./Kat Howard:
Kat Howard - Umbra magiei 1.0 '{Supranatural}.docx

./Kathrin Schmidt:
Kathrin Schmidt - N-ai sa mori 0.7 '{Literatura}.docx

./Kathryn Erskine:
Kathryn Erskine - Pasare cantatoare 1.0 '{Tineret}.docx

./Kathryn Wagner:
Kathryn Wagner - Dansatoarea lui Degas 0.99 '{Literatura}.docx

./Kathy Reichs:
Kathy Reichs - Oase frante 1.0 '{Thriller}.docx
Kathy Reichs - Oase incrucisate 1.0 '{Thriller}.docx

./Kathy Rung:
Kathy Rung - A inceput la Londra 0.99 '{Dragoste}.docx
Kathy Rung - Capitanul inimii 0.99 '{Dragoste}.docx
Kathy Rung - Foc si gheata 0.99 '{Dragoste}.docx
Kathy Rung - Marigold 0.99 '{Dragoste}.docx

./Katia Fox:
Katia Fox - Semnul de arama 0.9 '{AventuraIstorica}.docx

./Katie McGarry:
Katie McGarry - V1 Indrazneste sa iubesti 0.99 '{Literatura}.docx
Katie McGarry - V2 Destine la limita 1.0 '{Literatura}.docx
Katie McGarry - V3 Cu el pentru totdeauna 0.99 '{Literatura}.docx
Katie McGarry - V4 Mai puternici impreuna 1.0 '{Dragoste}.docx

./Katja Millay:
Katja Millay - Mare tranquillitatis 2.0 '{Diverse}.docx

./Katja Petrowskaja:
Katja Petrowskaja - Poate Estera 0.99 '{Literatura}.docx

./Katrina Raphaell:
Katrina Raphaell - Lumina cristalelor 0.8 '{Spiritualitate}.docx

./Katy Belair:
Katy Belair - Chemarea la rampa 0.7 '{Dragoste}.docx
Katy Belair - Dragostea nu se cumpara 0.99 '{Dragoste}.docx
Katy Belair - Fata guvernatorului 0.99 '{Dragoste}.docx

./Kaui Hart Hemmings:
Kaui Hart Hemmings - Descendentii 1.0 '{Literatura}.docx

./Kay Hooper:
Kay Hooper - Iubire sau nimic 0.9 '{Dragoste}.docx

./Kayla Olson:
Kayla Olson - Imperiul castelului de nisip 1.0 '{Literatura}.docx

./Kazuo Ishiguro:
Kazuo Ishiguro - Amintirea palida a muntilor 0.9 '{Literatura}.docx
Kazuo Ishiguro - Nemangaiatii 2.0 '{Literatura}.docx
Kazuo Ishiguro - Pe cand eram orfani 0.9 '{Literatura}.docx
Kazuo Ishiguro - Ramasitele zilei 0.99 '{Literatura}.docx
Kazuo Ishiguro - Sa nu ma parasesti 2.0 '{Literatura}.docx
Kazuo Ishiguro - Uriasul ingropat 1.0 '{Literatura}.docx

./Keith Douglass:
Keith Douglass - Ambuscada 1.0 '{SF}.docx
Keith Douglass - Atac 1.0 '{ActiuneComando}.docx
Keith Douglass - Commando 1.0 '{ActiuneComando}.docx
Keith Douglass - Misiune fulger 1.0 '{SF}.docx
Keith Douglass - USS Carrier - V1 Carrier 2.0 '{ActiuneComando}.docx
Keith Douglass - USS Carrier - V2 Vipera ataca 3.0 '{ActiuneComando}.docx
Keith Douglass - USS Carrier - V4 Batalia pentru Norvegia 1.0 '{ActiuneComando}.docx
Keith Douglass - USS Carrier - V16 Atac atac atac 1.0 '{ActiuneComando}.docx

./Keith Stuart:
Keith Stuart - Baiatul care voia sa-si construiasca lumea 1.0 '{Literatura}.docx

./Kelley Armstrong:
Kelley Armstrong - Femei din Alta Lume - V1 Luna plina 1.0 '{Supranatural}.docx
Kelley Armstrong - Femei din Alta Lume - V2 A doua luna plina 0.9 '{Supranatural}.docx
Kelley Armstrong - Fortele Raului Absolut - V1 Invocarea 0.9 '{Supranatural}.docx
Kelley Armstrong - Fortele Raului Absolut - V2 Revelatia 1.0 '{Supranatural}.docx
Kelley Armstrong - Fortele Raului Absolut - V3 Rafuiala 1.0 '{Supranatural}.docx

./Kelly Diamond:
Kelly Diamond - Alegerea 0.9 '{Romance}.docx
Kelly Diamond - Infruntand tigrul 0.99 '{Dragoste}.docx
Kelly Diamond - Vis de adolescenta 0.7 '{Dragoste}.docx

./Ken Bruen:
Ken Bruen - Jack Taylor - V1 Garda 0.9 '{Politista}.docx
Ken Bruen - Jack Taylor - V2 Moarte in clan 0.9 '{Politista}.docx
Ken Bruen - Jack Taylor - V3 Martirele de la Magdalen 0.9 '{Politista}.docx

./Ken Carey:
Ken Carey - Transmisiunile semintiei stelare 0.8 '{Spiritualitate}.docx

./Kendare Blake:
Kendare Blake - Anna in vesmant de sange 1.0 '{Supranatural}.docx
Kendare Blake - Trei coroane intunecate - V1 Trei coroane intunecate 1.0 '{Supranatural}.docx

./Ken Follett:
Ken Follett - Alb infinit 3.0 '{AventuraIstorica}.docx
Ken Follett - Al treilea geaman 1.0 '{SF}.docx
Ken Follett - Bani de hartie 1.0 '{AventuraIstorica}.docx
Ken Follett - Codul Rebecca 2.0 '{AventuraIstorica}.docx
Ken Follett - Copiii Edenului 1.0 '{AventuraIstorica}.docx
Ken Follett - Kingsbridge - V1 Stalpii Pamantului 4.0 '{AventuraIstorica}.docx
Ken Follett - Kingsbridge - V2 O lume fara sfarsit 2.0 '{AventuraIstorica}.docx
Ken Follett - Kingsbridge - V3 Coloana de foc 1.0 '{AventuraIstorica}.docx
Ken Follett - O avere periculoasa 0.9 '{Diverse}.docx
Ken Follett - Omul din Sankt Petersburg 1.0 '{AventuraIstorica}.docx
Ken Follett - Pe aripi de vultur 0.99 '{AventuraIstorica}.docx
Ken Follett - Prin urechile acului 1.0 '{AventuraIstorica}.docx
Ken Follett - Trilogia Secolului - V1 Caderea uriasilor 3.0 '{AventuraIstorica}.docx
Ken Follett - Trilogia Secolului - V2 Iarna lumii 2.0 '{AventuraIstorica}.docx
Ken Follett - Trilogia Secolului - V3 Capatul vesniciei 2.0 '{AventuraIstorica}.docx

./Ken Grimwood:
Ken Grimwood - Replay 3.0 '{SF}.docx

./Kenichi Yamamoto:
Kenichi Yamamoto - Enigma Rikyu 1.0 '{Literatura}.docx

./Kenize Mourad:
Kenize Mourad - In numele printesei moarte 1.0 '{Literatura}.docx

./Ken Kesey:
Ken Kesey - Zbor deasupra unui cuib de cuci 1.0 '{Literatura}.docx

./Ken Macleod:
Ken Macleod - Oamenii autostrazii 0.9 '{Diverse}.docx
Ken Macleod - Vanatorii de fulgere 1.0 '{SF}.docx

./Ken Mcclure:
Ken Mcclure - Trauma 1.0 '{Thriller}.docx

./Kenneth Grahame:
Kenneth Grahame - Vantul prin salcii 5.0 '{Copii}.docx

./Kenneth Oppel:
Kenneth Oppel - Ucenicia lui Viktor Frankenstein - V1 Elixirul vietii 0.9 '{Supranatural}.docx

./Kenneth Royce:
Kenneth Royce - Dansul marionetelor 1.0 '{Suspans}.docx
Kenneth Royce - Fiul ambasadorului 1.0 '{Suspans}.docx
Kenneth Royce - Pe urmele lui Iuda 1.0 '{Suspans}.docx
Kenneth Royce - Presedintele a murit 1.0 '{Suspans}.docx
Kenneth Royce - Umbre periculoase 1.0 '{Suspans}.docx

./Kenneth Wapnick:
Kenneth Wapnick - 01 - Curs de Miracole - Introducere generala la Cursul de Miracole 0.7 '{Spiritualitate}.docx
Kenneth Wapnick - 02 - Curs de Miracole - Curs de Miracole pentru studenti 0.99 '{Spiritualitate}.docx
Kenneth Wapnick - 03 - Curs de Miracole - Culegere de exercitii pentru studenti - Lectiile 1-365 0.2 '{Spiritualitate}.docx
Kenneth Wapnick - 04 - Curs de Miracole - Manual pentru profesori 0.8 '{Spiritualitate}.docx

./Ken Schooland:
Ken Schooland - Aventurile lui Jonathan Gullible 2.0 '{Filozofie}.docx

./Kenzaburo Oe:
Kenzaburo Oe - Fiinta sexuala 1.0 '{Erotic}.docx

./Kerry Drewery:
Kerry Drewery - Celula 7 1.0 '{Thriller}.docx
Kerry Drewery - Ziua 7 0.9 '{Thriller}.docx

./Kerry Lonsdale:
Kerry Lonsdale - Everything - V1 Viata pe care am visat-o 1.0 '{Dragoste}.docx

./Kerstin Gier:
Kerstin Gier - Barbati si alte catastrofe 1.0 '{Literatura}.docx
Kerstin Gier - Ce sa fac cu mostenirea 1.0 '{Literatura}.docx
Kerstin Gier - Culorile Dragostei - V1 Rosu de rubin 1.0 '{CalatorieinTimp}.docx
Kerstin Gier - Culorile Dragostei - V2 Albastru de safir 1.0 '{CalatorieinTimp}.docx
Kerstin Gier - Culorile Dragostei - V3 Verde de smarald 1.0 '{CalatorieinTimp}.docx
Kerstin Gier - Ma sinucid alta data 1.0 '{Literatura}.docx

./Kesariei Kapodokiei:
Kesariei Kapodokiei - Asezamintele 0.7 '{Religie}.docx

./Kesisian Bedros:
Kesisian Bedros - Cartea legii 0.99 '{Spiritualitate}.docx

./Ketherine Paterson:
Ketherine Paterson - Punte spre Terabithia 1.0 '{Tineret}.docx

./Kevin Brooks:
Kevin Brooks - Lucas 1.0 '{Dragoste}.docx

./Kevin Dockery:
Kevin Dockery - Trupele speciale in actiune 1.0 '{ActiuneComando}.docx

./Kevin J. Anderson:
Kevin J. Anderson - Specii indigene 1.0 '{SF}.docx

./Kevin J. Anderson & Doug Beason:
Kevin J. Anderson & Doug Beason - Sfarsitul 2.0 '{Aventura}.docx

./Kevin Mitnick:
Kevin Mitnick - Fantoma retelelor 1.0 '{Suspans}.docx

./Kevin Sands:
Kevin Sands - Cheia maestrului 1.0 '{SF}.docx

./Khaled Hosseini:
Khaled Hosseini - Si muntii au ecou 1.0 '{Literatura}.docx
Khaled Hosseini - Splendida cetate a celor o mie de sori 1.0 '{Literatura}.docx
Khaled Hosseini - Vanatorii de zmeie 1.0 '{Literatura}.docx

./Khoury Raymond:
Khoury Raymond - Ultimul templier 0.7 '{AventuraIstorica}.docx

./Kido Okamoto:
Kido Okamoto - Fiica negustorului de sake 1.0 '{Literatura}.docx

./Kiera Cass:
Kiera Cass - Alegerea - V1 Alegerea 1.0 '{Dragoste}.docx
Kiera Cass - Alegerea - V2 Elita 1.0 '{Dragoste}.docx
Kiera Cass - Alegerea - V3 Aleasa 1.0 '{Dragoste}.docx
Kiera Cass - Alegerea - V4 Mostenitoarea 1.0 '{Dragoste}.docx
Kiera Cass - Alegerea - V5 Coroana 1.0 '{Dragoste}.docx

./Kiersten White:
Kiersten White - Paranormal - V1 Paranormal 2.0 '{Supranatural}.docx
Kiersten White - Paranormal - V2 Supranatural 2.0 '{Supranatural}.docx
Kiersten White - Saga Cuceritorului - V1 Si ma intunec 1.0 '{Supranatural}.docx

./Kimberly Bennett:
Kimberly Bennett - Alaturi de Edward 1.0 '{Literatura}.docx

./Kimberly Bradley:
Kimberly Bradley - Razboiul care mi-a salvat viata 1.0 '{Razboi}.docx

./Kimberly Llewellyn:
Kimberly Llewellyn - Te rog frumos 0.99 '{Romance}.docx

./Kim Edwards:
Kim Edwards - Fiica tacerii 1.0 '{Literatura}.docx

./Kim Harrison:
Kim Harrison - Madison Avery - V1 Pe jumatate moarta 1.0 '{Supranatural}.docx
Kim Harrison - Madison Avery - V2 Moarta si inviata 1.0 '{Supranatural}.docx

./Kim Liggett:
Kim Liggett - Anul de gratie 1.0 '{Literatura}.docx

./Kim Stanley Robinson:
Kim Stanley Robinson - 2312 1.0 '{SF}.docx
Kim Stanley Robinson - Aurora 0.99 '{Literatura}.docx
Kim Stanley Robinson - Capital Code - V1 40 de semne de ploaie 2.0 '{SF}.docx
Kim Stanley Robinson - Capital Code - V2 50 de grade sub zero 2.0 '{SF}.docx
Kim Stanley Robinson - Saman 1.0 '{SF}.docx
Kim Stanley Robinson - Trilogia Marte 1.0 '{SF}.docx

./Kim Vogel Sawyer:
Kim Vogel Sawyer - Promisiunea primaverii 0.7 '{Diverse}.docx

./Kingsley Amis:
Kingsley Amis - Batranii ticalosi 1.0 '{Literatura}.docx
Kingsley Amis - Viata lui Mason 0.99 '{SF}.docx

./Kiran Desai:
Kiran Desai - Mostenitoarea taramului pierdut 1.0 '{Literatura}.docx

./Kir Buliciov:
Kir Buliciov - Viacik, lasa lucrurile-n pace 0.99 '{Diverse}.docx

./Kirill Buliciov:
Kirill Buliciov - Fetita de pe Terra 1.0 '{Tineret}.docx

./Kirk B. Ogden:
Kirk B. Ogden - Jocuri periculoase 1.0 '{ActiuneRazboi}.docx

./Kirk Douglas:
Kirk Douglas - Ultimul tango in Brooklyn 1.0 '{Romance}.docx
Kirk Douglas - V1 Dansul cu diavolul 1.0 '{Romance}.docx
Kirk Douglas - V2 Darul 1.0 '{Romance}.docx

./Kirsten Boie:
Kirsten Boie - Alhambra 1.0 '{AventuraIstorica}.docx

./Kirsten Cross:
Kirsten Cross - Lupii lui Skadi 1.0 '{SF}.docx

./Kirsten J. Bishop:
Kirsten J. Bishop - Orasul gravat 1.0 '{SF}.docx

./Kirsty Morgan:
Kirsty Morgan - Febra primaverii 0.9 '{Dragoste}.docx

./Kjell Olla Dahl:
Kjell Olla Dahl - Al patrulea 2.0 '{Politista}.docx

./Klaus Drobisch:
Klaus Drobisch - Rezistenta la Buchenwald 1.0 '{Razboi}.docx

./Klaus Kenneth:
Klaus Kenneth - Doua milioane de kilometri in cautarea adevarului 1.0 '{Literatura}.docx

./Knut Hamsun:
Knut Hamsun - Benoni. Rosa 2.0 '{Literatura}.docx
Knut Hamsun - Copii ai timpului lor 2.0 '{Literatura}.docx
Knut Hamsun - Foamea 2.0 '{Literatura}.docx
Knut Hamsun - Mistere 2.0 '{Literatura}.docx
Knut Hamsun - Pan. Victoria 1.0 '{Literatura}.docx
Knut Hamsun - Rodul pamantului 1.0 '{Literatura}.docx
Knut Hamsun - Ultimul capitol 2.0 '{Literatura}.docx
Knut Hamsun - Visatorii. Cer de toamna instelat 2.0 '{Literatura}.docx

./Kobo Abe:
Kobo Abe - Femeia nisipurilor 1.0 '{Literatura}.docx
Kobo Abe - Harta arsa 0.9 '{Literatura}.docx

./Koji Suzuki:
Koji Suzuki - Ring - V1 Cercul 1.0 '{Thriller}.docx
Koji Suzuki - Ring - V2 Spirala 1.0 '{Thriller}.docx

./Konstantin Fedin:
Konstantin Fedin - Sanatoriul Arkthur 1.0 '{Literatura}.docx

./Konstantin Paustovski:
Konstantin Paustovski - Trandafirul de aur 1.0 '{Literatura}.docx

./Konstantin Simonov:
Konstantin Simonov - Vii si morti 1.0 '{ActiuneRazboi}.docx

./Kostas Asimakopoulos:
Kostas Asimakopoulos - Generatia prizonierilor 2.0 '{ActiuneRazboi}.docx

./Kostas D. Kyriaziz:
Kostas D. Kyriaziz - Theofana, imparateasa Bizantului 1.1 '{AventuraIstorica}.docx

./Kostis Bastias:
Kostis Bastias - Actiunea Paianjen 44 1.0 '{ActiuneComando}.docx

./Krikor H. Zambaccian:
Krikor H. Zambaccian - Insemnarile unui amator de arta 1.0 '{Diverse}.docx

./Kristen Robinette:
Kristen Robinette - Moneda spaniola 0.99 '{Dragoste}.docx

./Kristina Ohlsson:
Kristina Ohlsson - Fredrika Bergman & Alex Recht - V1 Nedoriti 1.0 '{Politista}.docx
Kristina Ohlsson - Fredrika Bergman & Alex Recht - V2 Redusi la tacere 1.0 '{Politista}.docx

./Kristin Cashore:
Kristin Cashore - Cele 7 Regate - V1 Darul 1.0 '{SF}.docx
Kristin Cashore - Cele 7 Regate - V2 Fire, domnita monstru 1.0 '{SF}.docx

./Kristin Cast & P. C. Cast:
Kristin Cast & P. C. Cast - Casa Noptii - V1 Semnul 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V2 Tradarea 2.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V3 Aleasa 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V4 Infruntarea 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V5 Obsesia 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V6 Tentatia 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V7 Focul 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V8 Iertarea 1.0 '{Vampiri}.docx
Kristin Cast & P. C. Cast - Casa Noptii - V9 Predestinare 1.0 '{Vampiri}.docx

./Kristine Barnet:
Kristine Barnet - Scanteia 2.0 '{Diverse}.docx

./Kristine Ohsonne:
Kristine Ohsonne - O amnezie periculoasa 0.9 '{Dragoste}.docx

./Kristin Hannah:
Kristin Hannah - A doua sansa 1.0 '{Literatura}.docx
Kristin Hannah - Caderea ingerului 1.0 '{Dragoste}.docx
Kristin Hannah - Clipa magica 1.0 '{Literatura}.docx
Kristin Hannah - Dincolo de tacere 1.0 '{Literatura}.docx
Kristin Hannah - Drum in noapte 1.0 '{Literatura}.docx
Kristin Hannah - Firefly Lane - V1 Aleea cu licurici 1.0 '{Literatura}.docx
Kristin Hannah - Firefly Lane - V2 Dincolo de stele 1.0 '{Literatura}.docx
Kristin Hannah - Frontul de acasa 1.0 '{Literatura}.docx
Kristin Hannah - Gradina de iarna 1.0 '{Literatura}.docx
Kristin Hannah - In numele dragostei 1.0 '{Dragoste}.docx
Kristin Hannah - Intoarcerea acasa 1.0 '{Literatura}.docx
Kristin Hannah - Privighetoarea 1.0 '{Literatura}.docx
Kristin Hannah - Regasire 1.0 '{Literatura}.docx
Kristin Hannah - Un nou inceput 1.0 '{Dragoste}.docx

./Krystal Sutherland:
Krystal Sutherland - Inimi chimice 1.0 '{Dragoste}.docx
Krystal Sutherland - O lista semidefinitiva 1.0 '{Literatura}.docx

./Krystyna Berwinska:
Krystyna Berwinska - Con amore 1.0 '{Romance}.docx

./Kunio Tsuji:
Kunio Tsuji - Il signore 1.0 '{AventuraIstorica}.docx

./Kurt E. Koch:
Kurt E. Koch - Ocultismul si vindecarea sufletului 0.8 '{Spiritualitate}.docx

./Kurt Tucholsky:
Kurt Tucholsky - Castelul Gripsholm 1.0 '{Literatura}.docx

./Kurt Vonnegut:
Kurt Vonnegut - Abatorul cinci 2.0 '{SF}.docx
Kurt Vonnegut - Harrison Bergeron 0.6 '{SF}.docx
Kurt Vonnegut - Hocus Pocus 0.9 '{SF}.docx
Kurt Vonnegut - Leaganul pisicii 1.0 '{SF}.docx
Kurt Vonnegut - Pianul mecanic 1.0 '{SF}.docx
Kurt Vonnegut - Puscariasul 1.0 '{SF}.docx

./Kyung Sook Shin:
Kyung Sook Shin - Voi fi acolo 0.99 '{Literatura}.docx
```

